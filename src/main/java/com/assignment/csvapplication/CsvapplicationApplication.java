package com.assignment.csvapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.assignment.csvapplication")
public class CsvapplicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsvapplicationApplication.class, args);
	}

}
