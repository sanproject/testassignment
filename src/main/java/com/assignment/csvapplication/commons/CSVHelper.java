package com.assignment.csvapplication.commons;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import com.assignment.csvapplication.entity.CSVDataEntity;

public class CSVHelper {

	public static String TYPE = "text/csv";

	  public static boolean hasCSVFormat(MultipartFile file) {

	    if (!TYPE.equals(file.getContentType())) {
	      return false;
	    }

	    return true;
	  }

	  public static List<CSVDataEntity> convertCSVData(InputStream is) {
	    try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	        CSVParser csvParser = new CSVParser(fileReader,
	        		CSVFormat.DEFAULT.withFirstRecordAsHeader())) {

	      List<CSVDataEntity> csvDataList = new ArrayList<CSVDataEntity>();

	      Iterable<CSVRecord> csvRecords = csvParser.getRecords();

	      for (CSVRecord csvRecord : csvRecords) {
	    	  CSVDataEntity csvData = new CSVDataEntity(
	    		csvRecord.get("code"),
	              csvRecord.get("source"),
	              csvRecord.get("codeListCode"),
	              csvRecord.get("displayValue"),
	              csvRecord.get("longDescription"),
	              csvRecord.get("fromDate"),
	              csvRecord.get("toDate"),
	              csvRecord.get("sortingPriority")
	            );

	    	  csvDataList.add(csvData);
	      }

	      return csvDataList;
	    } catch (IOException e) {
	      throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
	    }
	  }

}
