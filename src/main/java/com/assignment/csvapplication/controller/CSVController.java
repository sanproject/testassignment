package com.assignment.csvapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.assignment.csvapplication.model.CSVDataDTO;
import com.assignment.csvapplication.model.CSVDataResponseListDTO;
import com.assignment.csvapplication.service.CSVService;


@RestController
@RequestMapping(("application/csv"))
public class CSVController {
	
	@Autowired
	CSVService CSVService;

	@PostMapping("/upload")
	public ResponseEntity<String> uploadCSV(@RequestParam(name= "file") MultipartFile file) {
		return CSVService.uploadData(file);
	}
	
	
	@GetMapping(value = "/all")
	public CSVDataResponseListDTO getAllData() {
		return CSVService.getAllData();
	}
	
	@GetMapping(value = "/{code}")
	public ResponseEntity<Object> getDataByCode(@PathVariable String code) {
		return CSVService.getDataByCode(code);
	}
	
	@DeleteMapping(value = "/all")
	public ResponseEntity<String> deleteData() {
		return CSVService.deleteAll();
	}
}
