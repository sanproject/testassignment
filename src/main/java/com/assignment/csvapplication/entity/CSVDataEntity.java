package com.assignment.csvapplication.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "csvdata")
public class CSVDataEntity {

	@Id
	private String code;
	private String source;
	private String codeListCode;
	private String displayValue;
	private String longDescription;
	private String fromDate;
	private String toDate;
	private String sortingPriority;
	
	
}
