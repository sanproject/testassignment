package com.assignment.csvapplication.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CSVDataDTO {

	private String codeListCode;
	private String source;
	private String code;
	private String displayValue;
	private String longDescription;
	private String fromDate;
	private String toDate;
	private String sortingPriority;
}
