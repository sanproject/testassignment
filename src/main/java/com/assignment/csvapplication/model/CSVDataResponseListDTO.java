package com.assignment.csvapplication.model;

import java.util.List;

import com.assignment.csvapplication.entity.CSVDataEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CSVDataResponseListDTO {

	private List<CSVDataEntity> dataList;
}
