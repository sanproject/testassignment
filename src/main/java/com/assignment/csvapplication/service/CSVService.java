package com.assignment.csvapplication.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.multipart.MultipartFile;

import com.assignment.csvapplication.commons.CSVHelper;
import com.assignment.csvapplication.entity.CSVDataEntity;
import com.assignment.csvapplication.model.CSVDataDTO;
import com.assignment.csvapplication.model.CSVDataResponseListDTO;
import com.assignment.csvapplication.service.repository.CSVRepository;



@Service
public class CSVService {
	
	@Autowired
	CSVRepository repo;

	public ResponseEntity<String> uploadData(MultipartFile file) {
		try {
			  if(CSVHelper.hasCSVFormat(file)) {
		      List<CSVDataEntity> csvData = CSVHelper.convertCSVData(file.getInputStream());
		      repo.saveAll(csvData);
		      return new ResponseEntity<>("The file uploaded successfully", HttpStatus.CREATED); 
			  }else {
				  return new ResponseEntity<String>("Invalid file format", HttpStatus.BAD_REQUEST);
			  }
		    } catch (IOException e) {
		      throw new RuntimeException("fail to store csv data: " + e.getMessage());
		    }
		    
		
	}

	public CSVDataResponseListDTO getAllData() {
		List<CSVDataEntity> dataList = new ArrayList<>();
		repo.findAll().forEach(dataList::add);
		return CSVDataResponseListDTO.builder().dataList(dataList).build();
		
	}
	
	public ResponseEntity<String> deleteAll() {
		repo.deleteAll();
		return new ResponseEntity<>("All data are deleted", HttpStatus.OK);
		
	}

	public ResponseEntity<Object> getDataByCode(String code) {
		Optional<CSVDataEntity> entity = repo.findById(code);
		if(entity.isPresent()) {
			CSVDataEntity data = entity.get();
			return new ResponseEntity<Object>(CSVDataDTO.builder().source(data.getSource()).codeListCode(data.getCodeListCode()).code(data.getCode()).displayValue(data.getDisplayValue())
					.longDescription(data.getLongDescription()).fromDate(data.getFromDate()).toDate(data.getToDate()).sortingPriority(data.getSortingPriority()).build(),HttpStatus.OK);
		}
		return new ResponseEntity<Object>("Data not Found",HttpStatus.NOT_FOUND);
	}
	
	
	

}
