package com.assignment.csvapplication.service.repository;

import org.springframework.data.repository.CrudRepository;

import com.assignment.csvapplication.entity.CSVDataEntity;

public interface CSVRepository extends CrudRepository<CSVDataEntity, String>{

}
